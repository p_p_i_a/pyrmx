from pyrmx.messages import CMD_START, Message, _ensure_bytes
from pyrmx.utils import checksum
import pytest

@pytest.fixture
def test_blob():
    return b''.join([CMD_START, #start command
                      b'\x4d', # the command byte
                      b'\x00\x07', #payload size (7) encoded as 2 bytes
                      b'my_data',
                      checksum(b'my_data')
    ])
@pytest.mark.parametrize('command_byte', [b'\x4d', 0x4d])
def test_building_data_message(command_byte, test_blob):
    payload = b'my_data'
    message = Message.build(command_byte, payload)
    assert message.as_bytes() == test_blob


def test_serializing_messages(test_blob):
    message = Message.from_binary(test_blob)
    assert message.payload_bytes == b'my_data'


@pytest.mark.parametrize(
    'x,expected', [
        (b'\x81', b'\x81'),
        (0x81, b'\x81'),
        ([0x81, 0x80], b'\x81\x80')
])
def test_ensuring_bytes(x, expected):
    assert _ensure_bytes(x) == expected
