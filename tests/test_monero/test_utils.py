from pyrmx.monero.utils import (
    Block, Transaction,
    TransactionBatch,
    AsyncBlockManager,
    BatchIndex,
    decode_varint)
from pyrmx.monero.node import PyrmxMoneroClient
import pyrmx.monero.utils
import pytest


def test_parsing_transaction(sample_json_txn):
    txn = Transaction(sample_json_txn, 1)
    assert len(txn.P_keys) != 0
    assert len(txn.R_keys) != 0
    assert isinstance(txn.P_keys[0], bytes)
    assert isinstance(txn.R_keys[0], bytes)
    assert len(txn.P_keys[0]) == 32
    assert len(txn.R_keys[0]) == 32
    assert txn['tx_hash'] == '13c8eb6269a5cf9a6865fb3ddfabb84232f59fa454125d5b9c904731c432a41d'

    tx_bytes = txn.as_rmx_bytes(0) #doesn't throw errs
    assert tx_bytes

def test_parsing_miner_transaction(sample_miner_json_txn):
    txn = Transaction(sample_miner_json_txn, tx_offset=0)
    assert len(txn.P_keys) != 0
    assert len(txn.R_keys) != 0
    assert isinstance(txn.P_keys[0], bytes)
    assert isinstance(txn.R_keys[0], bytes)
    assert txn['tx_hash'] == '27ad5c337f45666223402c01428974910bbca4c9ef2b7e29e4a9b16fe0fbb871'

def test_parsing_block(test_json_block):
    block = Block(test_json_block)
    txids = list(block.tx_hashes)
    # hardcoded miner tx and our tx from our stagenet block
    assert txids == [
        '27ad5c337f45666223402c01428974910bbca4c9ef2b7e29e4a9b16fe0fbb871',
        '13c8eb6269a5cf9a6865fb3ddfabb84232f59fa454125d5b9c904731c432a41d'
    ]

@pytest.mark.asyncio
async def test_creating_batches_from_block_manager(monero_rpc):
    max_size= 250
    start_bheight = 0
    start_tx_offset = 0
    bm = AsyncBlockManager(
        monero_rpc,
        bg_refresh_blockheight_delay=None)

    batch0 = await TransactionBatch.from_block_manager(
        bm, start_bheight, start_tx_offset, max_size)

    # make sure first tx in batch starts really where it's meant to
    assert (
        (batch0[0].block_height, batch0[0].tx_offset) ==
        (start_bheight, start_tx_offset)
    )
    next_batch_start = batch0.next_batch_index

    batch1 = await TransactionBatch.from_block_manager(
        bm, *next_batch_start, max_size=max_size)

    assert (batch1[0].block_height, batch1[0].tx_offset) == next_batch_start


@pytest.mark.parametrize(
    'extra, expected',
    # the 9999 is extra padding to make sure we don't parse it
    [
        ([1, 9999], (1, 1)),
        ([127, 9999], (127, 1)),
        # both start with 0 as msb so the first int is the value
        ([227, 1, 9999], (227, 2))
        # thats 1110 0011 (=227) and 0000 0001
        # if we get rid of each msb we have bits
        # _110 0011  _000 0001
        # reversed that is 000 0001 110 0011
        #https://carlmastrangelo.com/blog/lets-make-a-varint
        # https://monero.fandom.com/wiki/Varint
    ]
)
def test_decoding_varints(extra, expected):
    assert decode_varint(extra) == expected

def test_parsing_R_keys_from_extra():
    # stagenet block 282032 transaction 1
    extra = [
        1, 73, 250, 159, 186, 223, 28, 26, 77, 5, 76, 108, 191, 145, 135, 120,
        248, 105, 173, 2, 16, 53, 1, 216, 218, 126, 236, 135, 98, 235, 34, 2,
        230, 4, 4, 35, 167, 176, 156, 64, 120, 245, 201, 234, 87, 30, 61, 182,
        229, 64, 109, 231, 194, 127, 94, 158, 194, 124, 132, 87, 92, 231, 173,
        36, 123, 175, 3, 201, 185, 21, 41, 37, 236, 26, 60, 179, 175, 222, 59,
        164, 61, 66, 88, 31, 3, 43, 253, 57, 186, 219, 23, 128, 88, 246, 84,
        199, 75, 134, 13, 244, 243, 166, 213, 195, 189, 122, 191, 155, 76, 24,
        60, 93, 179, 212, 174, 52, 205, 46, 191, 231, 123, 225, 18, 255, 40,
        248, 105, 220, 46, 86, 0, 53, 19, 111, 32, 101, 149, 166, 119, 123,
        163, 190, 142, 218, 107, 175, 3, 109, 66, 95, 255, 224, 184, 103, 140,
        76, 165, 204, 133, 63, 112, 86, 148
    ]
    keys = Transaction._get_R_keys_from_extra(extra)
    assert len(keys) == 4 + 1
    assert all(len(k) == 32 for k in keys)

@pytest.mark.asyncio
async def test_block_manager(monero_rpc):
    # make sure that we can cache blocks

    bm = pyrmx.monero.utils.AsyncBlockManager(
        monero_rpc,
        bg_refresh_blockheight_delay=None)

    blocks = [10000, 10001, 10002]
    assert not bm._blocks
    assert not bm._future_blocks

    for bh in blocks:
        bm.schedule_block(bh)

    assert list(sorted(bm._future_blocks.keys())) == blocks

    for bh in blocks:
        await bm.get_block(bh)
        assert bh in bm._blocks
        assert bh not in bm._future_blocks

    assert 10000 in bm._blocks
    bm.delete_block(10000)
    assert 10000 not in bm

    assert 999 not in bm
