import os
import pytest
import json
from pathlib import Path
from collections import deque
from pyrmx.pyrmxd import PyRMXDaemon
THIS_FILE = Path(os.path.dirname(os.path.abspath(__file__)))


@pytest.fixture
def test_json_block():
    # s
    return {
        'blob': '0909fa9cdbe205b0a5aba10343fa914de436677522fb9093b3f1a6b1e47e725e589d678ca316101099489002f9ef0f01ffbdef0f01f0cb948998830502b08462c59232a7ae934c37d9ddb496daf39dd749c7d42ed627cd226039037c4d2101b7711bc8f6dce4cf54a84d49dd48c107efc64c00fdf1a103bdf55592c2465056000113c8eb6269a5cf9a6865fb3ddfabb84232f59fa454125d5b9c904731c432a41d',
        'block_header': {
            'block_size': 52421,
            'block_weight': 52421,
            'cumulative_difficulty': 10615235128,
            'depth': 24906,
            'difficulty': 20779,
            'hash': '464dcc0e2e146c0bf9c43cf068132f863075d5059c79032856517ce75857cbfb',
            'height': 260029,
            'long_term_weight': 52421,
            'major_version': 9,
            'minor_version': 9,
            'miner_tx_hash': '27ad5c337f45666223402c01428974910bbca4c9ef2b7e29e4a9b16fe0fbb871',
            'nonce': 2420676880,
            'num_txes': 1,
            'orphan_status': False,
            'pow_hash': '',
            'prev_hash': 'b0a5aba10343fa914de436677522fb9093b3f1a6b1e47e725e589d678ca31610',
            'reward': 22099773433328,
            'timestamp': 1549192826
        },
        'json': {
            'major_version': 9,
            'minor_version': 9,
            'timestamp': 1549192826,
            'prev_id': 'b0a5aba10343fa914de436677522fb9093b3f1a6b1e47e725e589d678ca31610',
            'nonce': 2420676880,
            'miner_tx': {
                'version': 2,
                'unlock_time': 260089,
                'vin': [{'gen': {'height': 260029}}],
                'vout': [
                    {
                        'amount': 22099773433328,
                        'target': {
                            'key': 'b08462c59232a7ae934c37d9ddb496daf39dd749c7d42ed627cd226039037c4d'
                        }
                    }
                ],
                'extra': [
                    1, 183, 113, 27, 200, 246, 220, 228, 207, 84, 168, 77, 73,
                    221, 72, 193, 7, 239, 198, 76, 0, 253, 241, 161, 3, 189,
                    245, 85, 146, 194, 70, 80, 86
                ],
                'rct_signatures': {'type': 0}
            },
            'tx_hashes': [
                '13c8eb6269a5cf9a6865fb3ddfabb84232f59fa454125d5b9c904731c432a41d'
            ]
        },
        'miner_tx_hash': '27ad5c337f45666223402c01428974910bbca4c9ef2b7e29e4a9b16fe0fbb871',
        'status': 'OK',
        'tx_hashes': ['13c8eb6269a5cf9a6865fb3ddfabb84232f59fa454125d5b9c904731c432a41d'],
        'untrusted': False
    }

@pytest.fixture
def sample_json_txn():
    # result of
    # tx_hashes = [
    #    '13c8eb6269a5cf9a6865fb3ddfabb84232f59fa454125d5b9c904731c432a41d'
    #    ]
    # the txns of our stagenet block
    # PyrmxMoneroClient.get_transactions(
    #    block['json']['tx_hashes'],
    #    as_json=True)
    with open(THIS_FILE / 'sample_txn.json') as inf:
        # return [json.loads(x) for x in json.load(inf)]
        return json.load(inf)[0]

@pytest.fixture
def sample_miner_json_txn():
    # result of
    # tx_hashes = [
    #    '13c8eb6269a5cf9a6865fb3ddfabb84232f59fa454125d5b9c904731c432a41d'
    #    ]
    # the txns of our stagenet block
    # PyrmxMoneroClient.get_transactions(
    #    block['json']['tx_hashes'],
    #    as_json=True)
    with open(THIS_FILE / 'sample_miner_txn.json') as inf:
        return json.load(inf)[0]

class MockPyRMXDaemon(PyRMXDaemon):
    def __init__(self, messages):
        self.context = {}
        self.messages = deque(messages)
        self.responses = deque()

    async def read_message(self):
        return self.messages.popleft()

    def send_bytes(self, response):
        self.responses.append(response)

class MockPyRMXDevice:
    pass


@pytest.fixture
def mock_pyrmxd():
    def factory(messages):
        return MockPyRMXDaemon(messages)
    return factory
