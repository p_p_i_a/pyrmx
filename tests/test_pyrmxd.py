import logging

import pytest
from pyrmx.pyrmxd import register_handler, HANDLERS, handle_debug_message
from pyrmx.pb_schemas.system_messages_pb2 import OutDebug

@pytest.mark.skip
def test_registering_handlers():
    class DummyPB:
        attr = 42

    @register_handler(b'\xff', DummyPB)
    def handle_xff(pb):
        return pb


    assert len(HANDLERS) == 1
    handler = HANDLERS[b'\xff']
    assert handler.callback == handle_xff
    assert handler.proto == DummyPB

    instance = DummyPB()
    ret = handle_xff(instance)
    assert ret.attr == 42

@pytest.mark.asyncio
async def test_debug_message(caplog):
    request = OutDebug()
    request.message = b'jelenovipivonelej'

    with caplog.at_level(logging.DEBUG):
        resp_byte, resp = await handle_debug_message(request, {})
        assert 'jelenovipivonelej' in caplog.text
        assert 'Debug message from RMX' in caplog.text
