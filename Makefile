proto:
	find . -name '*.proto' | xargs protoc --python_out=.

test:
	python -m pytest
