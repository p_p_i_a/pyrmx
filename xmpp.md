# POC implementation - workflows
Check `pb_schemas/xmpp_messneger.proto`
## Login type jid&password into rmx
First step to anything with messenger is to log-in

RMX sends
```
message OutXMPPLogin{
  required string jid            = 1;
  required string passwd         = 2;
  requried bytes  pubkey         = 3;
}
```
with `jid` (jabber-ID), password and the pubkey. The credentials can be typed on the keyboard or loaded from persistent memory

PyRMX
- connects to the server and creates an XML stream.
- (the username/password can be replaced with some challenge/response system) in order not to leak credentials to the pc
- publishes the pubkey into the owner's vCard (so others can download it for encrpytion)
- Responds with
```

message InXMPPLoginResponse{
  enum StatusCode  {
    ERROR = 0;
    OK = 1;
  }
  required StatusCode status_code  = 1;
  string message                   = 2;
}
```
the `message` optionally contains the errors (network error, bad credentials,...)

The RMX is now authenticated with the XMPP server and can now perform actions (send&receive messages)

## Adding contacts
Before we can exchange messages with anyone, we must mutually subscribe with each other.

RMX requests this
```
message OutAddContact {
 string jid = 1;
}
```
for example, I `thehood@sherwood.org` want to add `sheriff@server.org` to my contact list

PyRMX responds with
```
message InAddContact {
 string jid = 1;
 bytes  pubkey = 2;
}
```
Under the hood
1. the pubkey of `sheriff@server.org` is retreived from the vCard. If sheriff did not publish his pubkey, the field is left empty
2. a subscribe presence is sent to `sheriff@server.org`

at this point, RMX should save the jid + pubkey to persistent memory (although it can be re-requested each time)

## Sending messages RMX -> out
We now have `sheriff@server.org` in our contact list, so we can send him an encrypted message

RMX sends

```
message OutMessage{
  required string to_jid  = 1;
  required bytes  message = 2;
}
```
where `to_jid=sheriff@server.org` and   `message=encrypted_blob_of_a_message`

PyRMX, fowards this message to the server (we are already authenticated at this point) and responds with
```
message OutMessageStatus{
  required string to_jid = 1;
  enum Status {
    ERROR = 0;
    OK = 1;
  }
  required Status status = 2;
  string  desc = 3;
}
```
the `desc` contains optional human readable error description (couldn't send for network error or whatever reasons)

## Downloading messages in -> RMX
User selects `receive` messages in the menu.
The inbox is structured such that it shows the number of unread messages for each contact in the list. If one wishes to read the messages, they are requested one by one.

RMX sends
```
message OutQueryUnread {
  required repeated string for_jids = 1;
}
```

for example  `OutQueryUnread(for_jids=["sheriff@sherwood.org", "foo@bar.org"])`
would mean that the user want's to know how many unread messages there are for JIDs `sheriff@sherwood.org` and `foo@bar.org`.
PyRMX responds with

```
/* number of unread messages per JID*/
message InQueryUnread {
  map<string, uint32> unreads_count = 1;
}
```
a mapping `JID -> count_of_unread_messages` for example 
```
InQueryUnread.unreads_count = {
   "sheriff@sherwood.org": 4,
   "foo@bar.org": 0
}
```

at this point the RMX user wants to read the unread messages from `sheriff@sherwood.org` so he sends
```
message OutQueryMessage {
   required string from_jid = 1;
   }
```
PyRMX responds with the oldest unread message for this recipient

```
message InMessage {
  string from_jid    = 1;
  bytes message      = 2;
  uint32 timestamp   = 3; 
}
```
if `message` and `timestamp` are `null`, it means there are no more messages left.

# Setting up a testing xmpp server
prosody TODO!

# Architecture (how xmpp works) - not important
1. The XMPP service connects on behalf of the user (credentials imputed via rmxhw)
## Sending messages
## Receiving messages
- checks if any (??) new messages were delivered for the user AND/OR downloads last X messages

## Presence - full JID
```
xmpp:thehood@jabb.im/rmx
                     ^+^
                 |
                  presence 
```
to indicate capabilities (very limited in rmx)

## vCard-temp [XEP-0054]
to share pubkeys

IQ-get/set

## Blocking users
Because the XMPP developers care about privacy, they have defined an extension for communications blocking (defined in Privacy Lists [XEP-0016]), as well as a stripped- down interface to privacy lists (defined in Simple Communications Blocking [XEP-0191]).

## Presence
to indicate being online
requires approval from both parties
not needed for now?

## Roster - a contact list?
should be used, but how?

## IQ - GET/POST
managing contacts


## Message Archiving [XEP-0136]/[XEP-0313]
defines a technology for storing messages on your server instead of archiving them to your local machine. There are many scenarios in which this is helpful: perhaps you are using a web client that does not have local storage, the device you are using (e.g., a PDA or mobile phone) has limited storage capacity, or you move between different devices quite a bit and you want all of your message history in one place.

## Discovery - info disco#info

## Authentication
pass password over usb to pyrmx in the clear for now, but xmpp SASL supports alternative authentication methods (challenges) so we could pass the challenge to pyrmx and get back the response so it can be sent to the server without revealing the password
