from typing import List, Union, Tuple
import math
import asyncio
import queue
from binascii import unhexlify, hexlify
from pyrmx.exceptions import (
    MoneroRPCError,
    TransactionBatchSizeReached,
    BlockheightNotFound,
)
from collections import namedtuple
import logging

from io import BytesIO

logger = logging.getLogger(__name__)


BatchIndex = namedtuple("BatchIndex", "blockheight,tx_offset")


class Block(dict):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._transactions = None

    @property
    def tx_hashes(self):
        return [self["block_header"]["miner_tx_hash"]] + self["json"]["tx_hashes"]

    @property
    def transactions(self):
        if self._transactions is None:
            raise ValueError("No transactions available. Set them first")
        return self._transactions

    @transactions.setter
    def transactions(self, txns: List["Transaction"]):
        # todo type checking
        self._transactions = txns

    @property
    def height(self):
        return self["block_header"]["height"]

    def __str__(self):
        return "Block height {}".format(self.height)


class Transaction(dict):
    def __init__(self, tx_json, tx_offset: int):
        """
        Args:
            tx_json (dict): result of MoneroClient.get_transactions()['txs'][i]

        """
        super().__init__(tx_json)
        self.tx_offset = tx_offset

    @property
    def block_height(self):
        return self["block_height"]

    @property
    def tx_hash(self):
        return self["tx_hash"]

    @staticmethod
    def _get_R_keys_from_extra(extra):
        """Get R key from block 'extra' data

        The output public key P is in the json as vout.target.key.
        The transaction public key R is not separately listed, so you'll need to
        parse the extra field in the json and locate the txpubkey subfield.

        based on https://monero.stackexchange.com/questions/11035/which-two-strings-i-need-to-get-from-tx-json-if-i-want-to-determine-whether-out?newreg=20b3ab3ac5624529a1ab145732b60f91

        Args:
            extra: A list of integers as received from monerod RPC call result.extra

        Returns:
            a list of 32B numbers representing the public keys
                (the order corresponds to those in vout)
        """

        keys = []
        i = 0

        ba = bytearray(extra)
        while i < len(extra):
            tag = extra[i]
            if tag == 1:
                # pub key
                i += 1
                k = bytes(extra[i : i + 32])
                keys.append(k)
                i += 32
            elif tag == 0:
                i += 1
            elif tag == 4:
                # https://github.com/monero-project/monero/blob/master
                # /src/cryptonote_basic/tx_extra.h#L41
                # looks like the next tag is a varint telling us how many 32B pubkeys
                # there are.
                # https://monero.stackexchange.com/questions/11733/how-to-parse-tx-extra-tag-additional-pubkeys-from-extra
                i += 1
                num_pubkeys, varint_size = decode_varint(extra[i:])
                i += varint_size
                for pk in range(num_pubkeys):
                    keys.append(bytes(extra[i : i + 32]))
                    i += 32
            else:
                # other
                i += 1
                sz, varint_size = decode_varint(extra[i:])
                i += sz + varint_size
        return keys

    def as_rmx_bytes(self, offset_in_batch):
        # according to sources/communication.h in rmx-hardware-wallet
        p_keys = self.P_keys
        r_keys = self.R_keys
        return b"".join(
            [
                b"\x81",  # self.header_byte,
                offset_in_batch.to_bytes(1, byteorder="little"),
                len(r_keys).to_bytes(1, byteorder="little"),
                len(p_keys).to_bytes(1, byteorder="little"),
                *r_keys,
                *p_keys,
            ]
        )

    @property
    def P_keys(self):
        return [bytes.fromhex(out["target"]["key"]) for out in self["as_json"]["vout"]]

    @property
    def R_keys(self):
        return self._get_R_keys_from_extra(self["as_json"]["extra"])

    def __str__(self):
        return "Transaction(blockheight={}, block_offset={})".format(
            self.block_height, self.tx_offset
        )


class TransactionBatch:
    """A list of Transaction objects
    enabling lookup by index and having helper
    methods to serialize them to binary format that rmx expects.

    It's not a list subsclass because that would allow to modify the underlying
    transactions on the fly which could have unexpected results so I expose
    just several methods
    """

    def __init__(self, max_size=1024):
        self._bytes_payload = b""
        self._transactions = []
        self.max_size = max_size  # bytes
        self.next_batch_index = BatchIndex(None, None)

    def __str__(self):
        return "Batch of {} transactions of size {}/{}B".format(
            len(self), self.size_bytes(), self.max_size
        )

    @classmethod
    async def from_block_manager(
        cls,
        bm: "AsyncBlockManager",
        start_blockheight: int,
        start_tx_offset: int,
        max_size,
    ):
        batch = cls(max_size=max_size)
        want_height = start_blockheight
        want_offset = start_tx_offset

        while True:
            try:
                block = await bm.get_block(want_height)
            except BlockheightNotFound as err:
                logger.debug(err)
                batch.next_batch_index = BatchIndex(want_height, want_offset)
                logger.debug(
                    "Batch %s created. Next batch should start at %s",
                    batch,
                    batch.next_batch_index,
                )
                return batch
            for txn in block.transactions[want_offset:]:
                try:
                    batch.add_transaction(txn)
                except TransactionBatchSizeReached:
                    logger.debugvv("Can't add any more transactions to %s", batch)
                    batch.next_batch_index = BatchIndex(txn.block_height, txn.tx_offset)
                    return batch
            want_height += 1
            want_offset = 0
        return batch

    def __getitem__(self, ix):
        return self._transactions[ix]

    def __len__(self):
        return len(self._transactions)

    def _append(self, txn):
        self._transactions.append(txn)

    def size_bytes(self):
        """Size of txns serialized in binary fmt (without padding)"""
        return len(self._bytes_payload)

    @property
    def index(self):
        try:
            first_tx = self[0]
            return BatchIndex(first_tx.block_height, first_tx.tx_offset)
        except IndexError:
            return None

    def add_transaction(self, txn: Transaction):
        logger.debugvv("Adding transaction %s to a batch", txn)
        txn_bytes = txn.as_rmx_bytes(offset_in_batch=len(self))
        if len(txn_bytes) <= self.bytes_left():
            self._append(txn)
            self._bytes_payload += txn_bytes
        else:
            raise TransactionBatchSizeReached(
                "Can't fit transaction %s into batch", txn
            )

    def bytes_left(self):
        return self.max_size - self.size_bytes()

    def as_rmx_bytes(self):
        """Padded serialized txn PR keys as bytes"""
        return self._bytes_payload + b"\xff" * self.bytes_left()


def decode_varint(stream: List[int]) -> Tuple[int, int]:
    """Lazily decodes a stream of VARINTs to Python integers.

    Args:
        stream: List of ints (this is what monero transaction extra contains)
    Returns:
        tuple (int, size_of_the_varint)
        the first varint as integer
        # https://monero.fandom.com/wiki/Varint
    """
    value = 0
    base = 1
    i = 0
    while True:
        integer = stream[i]
        value += (integer & 0x7F) * base
        if integer & 0x80:
            # The MSB was set; increase the base and iterate again, continuing
            # to calculate the value.
            base *= 128
            i += 1
        else:
            # The MSB was not set; this was the last byte in the value.
            return value, i + 1


class AsyncBlockManager:
    """Asynchronous interface for getting specific blocks (=transactions)

    Enables scheduling this in the background
    """

    def __init__(self, rpc_client, bg_refresh_blockheight_delay=120):
        self.rpc_client = rpc_client
        self._future_blocks = {}
        self._blocks = {}

        self.bg_refresh_blockheight_delay = bg_refresh_blockheight_delay

        self._global_blockheight = math.inf

        self._bg_refresh_global_blockheight_job = asyncio.create_task(
            self._infinite_refresh_global_blockheight()
        )

    def __contains__(self, bh):
        return bh in self._blocks or bh in self._future_blocks

    def current_max_blockheight(self):
        """What is the latest cached/available blockheight"""
        bh = max(max(self._blocks, default=-1), max(self._future_blocks, default=-1))
        return bh

    @property
    def global_blockheight(self):
        return self._global_blockheight - 1  # -1 as a safety buffer

    async def _infinite_refresh_global_blockheight(self):
        logger.debug("Refreshing global blockheight in background")
        while self.bg_refresh_blockheight_delay:
            self._global_blockheight = (await self.rpc_client.get_info())["result"][
                "height"
            ]
            logger.info("New global blockheight is %s", self._global_blockheight)
            await asyncio.sleep(self.bg_refresh_blockheight_delay)
        logger.debug("Stopped refreshing global blockheight")

    def schedule_block(self, blockheight):
        """
        Fetch block in background if not already ready or scheduled
        """
        if blockheight not in self._blocks and blockheight not in self._future_blocks:
            if blockheight > self.global_blockheight:
                raise BlockheightNotFound(
                    "Height {} not available in {} current_max height is {}".format(
                        blockheight, self, self.global_blockheight
                    )
                )
            logger.debugv("Getting block %s in background", blockheight)
            future_block = asyncio.create_task(
                self.rpc_client.get_block_and_transactions(blockheight)
            )
            self._future_blocks[blockheight] = future_block

    async def get_block(self, blockheight):
        try:
            # check if block is ready
            block = self._blocks[blockheight]
        except KeyError:
            # schedules a block if not already scheduled
            self.schedule_block(blockheight)
            future_block = self._future_blocks.pop(blockheight)
            block = await future_block
            self._blocks[blockheight] = block
        return block

    def delete_block(self, blockheight):
        logger.debugv("Removing block %s from cache", blockheight)
        try:
            future_block = self._future_blocks.pop(blockheight)
        except KeyError:
            pass
        else:
            if not future_block.cancelled():
                future_block.cancel()
        try:
            del self._blocks[blockheight]
        except KeyError:
            pass


class AsyncBatchManager:
    def __init__(self, block_manager: AsyncBlockManager):
        self._block_manager = block_manager
        self._batches = {}
        self._future_batches = {}

    def schedule_batch(self, index: BatchIndex, size: int):
        # we have to index not just by blockheight and tx offset but size as well
        target_index = (*index, size)
        if target_index in self._future_batches or target_index in self._batches:
            return

        future_batch = asyncio.create_task(
            TransactionBatch.from_block_manager(
                self._block_manager, *index, max_size=size
            )
        )
        self._future_batches[(*index, size)] = future_batch

    async def get_batch(self, index: BatchIndex, size: int, prefetch_next: bool = True):
        want_index = (*index, size)
        try:
            return self._batches[want_index]
        except KeyError:
            # if it's already scheduled this skips the step
            self.schedule_batch(index, size)
            future_batch = self._future_batches.pop(want_index)
            batch = await future_batch
            self._batches[want_index] = batch
        if prefetch_next:
            next_ix = batch.next_batch_index
            logger.debugvv("Prefetching next batch %s", next_ix)
            try:
                self.schedule_batch(next_ix, size)
            except BlockheightNotFound as err:
                logger.info(err)
        return batch

    def delete_batch(self, index: BatchIndex, size: int):
        final_ix = (*index, size)
        try:
            fut = self._future_batches.pop(final_ix)
            if not fut.cancelled():
                fut.cancel()
        except KeyError:
            pass

        try:
            del self._batches[final_ix]
        except KeyError:
            pass
