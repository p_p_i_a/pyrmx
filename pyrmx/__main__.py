from pyrmx.pyrmxd import PyRMXDaemon
import asyncio
import logging
from argparse import ArgumentParser
import time
from pyrmx.exceptions import RmxHwNotConnected
from pyrmx.monero.node import PyrmxMoneroClient
import os
import sys
import pkg_resources

try:
    VERSION = pkg_resources.require("pyrmx")[0].version
except:
    VERSION = "unknown"


if __name__ == "__main__":
    logger = logging.getLogger(__name__)
    parser = ArgumentParser(
        prog="pyrmx", description="RMX hardware wallet client {}".format(VERSION)
    )
    parser.add_argument(
        "--monerod",
        action="store",
        required=False,
        default="http://127.0.0.1:38081",
        help="Monero node url (default: http://127.0.0.1:38081)",
    )
    parser.add_argument("--version", action="store_true", help="Show installed version")

    parser.add_argument(
        "-v",
        "--verbose",
        action="count",
        default=0,
        help="Logging verbosity -vvv contains the most details",
    )
    args = parser.parse_args()

    if args.version:
        print("pyrmx", VERSION)
        sys.exit(0)

    _verbosity = min(args.verbose, 3)
    _logging_level = {
        0: logging.INFO,
        1: logging.DEBUG,
        2: logging.DEBUGV,
        3: logging.DEBUGVV,
    }[_verbosity]
    logging.basicConfig(
        level=_logging_level,
        stream=sys.stderr,
        format="%(filename)s;%(levelname)s;%(asctime)s %(message)s",
    )

    urllib_logger = logging.getLogger("urllib3")
    urllib_logger.propagate = False
    if _verbosity <= 1:
        # disable slixmpp logging for info&debug levels
        slixmpp_logger = logging.getLogger("slixmpp")
        slixmpp_logger.propagate = False

    loop = asyncio.get_event_loop()
    while True:
        logger.info("Looking for RMX device")
        try:
            context = {"xmr_node": PyrmxMoneroClient(args.monerod)}
            d = PyRMXDaemon(context=context)
            break
        except RmxHwNotConnected:
            time.sleep(1)
    try:
        loop.run_until_complete(d.run())
    finally:
        d.device.close()
